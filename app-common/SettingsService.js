const conf = require('../config');

const ConfigService = function(config){};

ConfigService.prototype.dbConfig= function() {
    return conf.db;
};
ConfigService.prototype.sessionConfig = function () {
    return conf.sessions;
};
ConfigService.prototype.staticDir = function () {
  return  conf.staticFolder;
};
module.exports = new ConfigService();
