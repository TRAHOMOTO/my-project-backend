class User {
    constructor({ uid, login, roles = []}){
        this.uid = uid;
        this.login = login;
        this.roles = roles;
    }
}

module.exports = User;
