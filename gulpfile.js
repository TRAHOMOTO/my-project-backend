const gulp = require('gulp');
const gmcfp = require('gulp-mysql-command-file-processor');

const confService = require('./app-common/SettingsService');
const { database, user, password, host, port } = confService.dbConfig();

gulp.task('schema',function(cb){
    gulp.src('sql/db_schema.sql').pipe(gmcfp( user, password, host, port, 'MED', database));
    cb()
});