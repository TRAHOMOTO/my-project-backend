'use strict';
const PORT = process.env.PORT || 4202;

const API_VER = '/api_v1';
const QUERY_SELECT_USER_BY_ID = 'SELECT uid, login, roles FROM `users_with_roles` WHERE `uid`=?;';
const QUERY_SELECT_USER_BY_CREDENTIALS = 'SELECT uid, login FROM `users` WHERE `login`=? AND `password`=MD5(?) LIMIT 1;';

const {
    express,
    bodyParser,
    session,
    db,

    config,
    User} = require('./app-common');

const anonymous = new User({uid: 0, login: ''});

// Main middlewares ------------------------------------------------
const retrieveUserById = function (uid) {
    return new Promise((resolve, reject) => {
        let dbConn;
        db.createConnection(config.dbConfig())
            .then(conn => {
                dbConn = conn;
                return conn.query(QUERY_SELECT_USER_BY_ID, [uid]);
            })
            .then( rows => {
                if(rows.length){
                    const { uid, login, roles } = rows[0];
                    const user = new User({uid, login, roles: roles.split(',')});
                    resolve({ success: true, user });
                } else {
                    reject({ success: false, message: 'User not found' });
                }
            })
            .catch(err => console.error(err))
            .then(() => {
                if(dbConn && dbConn.state !== 'disconnected') dbConn.end();
            })
    });
};

const userRequestHandler = function (req, res) {
    retrieveUserById(req.session.uid)
        .then(result => {
            if(result.success){
                res.json(result.user);
            } else {
                res.status(400).send(result.message);
            }
        }).catch(err => {
            console.error(err);
            res.status(400).send(err.message);
        });
};

const authorisation = function (req, res, next) {
    if (req.session.uid) {
        return next();
    } else {
        return res.status(403).send('Access denied');
    }
};

const authentication = function (req, res, next) {
    if (req.session.uid) {
        return next(); // -> to success authentication handler
    }
    if (!req.body.login || !req.body.password) {
        return res.status(400).send('No credentials received');
    }
    let dbConn;
    db.createConnection(config.dbConfig())
        .then(conn => {
            dbConn = conn;
            return conn.query(
                QUERY_SELECT_USER_BY_CREDENTIALS,
                [req.body.login, req.body.password]
            );
        })
        .then(rows => {
            if(rows.length){
                req.session.uid = rows[0].uid;
                next(); // -> to success authentication handler
            } else {
                res.status(403).send('Unknown user');
            }
        })
        .catch(err => next(err)) // -> to authentication err handler
        .then(() => {
            if(dbConn && dbConn.state !== 'disconnected') dbConn.end();
        });
};

// Application bootstrapping ---------------------------------

const app = express();

app.use(session(config.sessionConfig()));
app.use(bodyParser.json());
app.use(express.static(config.staticDir()));

// Application endpoints -------------------------------------

/**
 * LogIn
 */
app.use(`${API_VER}/login`, authentication);
app.post(`${API_VER}/login`, userRequestHandler);
/**
 * Logout
 */
app.all(`${API_VER}/logout`, authorisation, (req, res) => {
    req.session.destroy();
    res.json({success: true, message: 'Bye-bye!'});
});
/**
 * Get current authenticated user
 */
app.all(`${API_VER}/user`, (req, res) => {
    if (req.session.uid) userRequestHandler(req, res);
    else res.json(anonymous);
});

/**
 * Some secured content
 */
app.get(`${API_VER}/secure`, authorisation, (req, res) => {
    res.send('Top secret!');
});

/**
 * Entry point for frontend
 */
app.get('/*', (req, res) => {
    res.redirect('/');
});
app.all('/', (req, res, next) => res.sendStatus(405));

/**
 * Global error handler
 */
app.use((err, req, res, next) => {
    console.error(err.message);
    res.sendStatus(500);
});

// Rock'n'Roll
app.listen(PORT, () => console.log(`Server listen on port: ${PORT}`));
