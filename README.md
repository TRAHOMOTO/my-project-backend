# MyProject
This project demonstrate approach to build simple application using **Angular 2** for frontend and **Node.JS** as backend.
This repository does't contains sources for frontend SPA, it can be founded 
in [**appropriate repository**](https://bitbucket.org/TRAHOMOTO/my-project-frontend)

## Requirements
1. [Node.JS 6.x](https://nodejs.org/en/download/) and [NPM 3.x](https://nodejs.org/en/download/)
2. [Mysql 5.x](https://dev.mysql.com/downloads/mysql/)

## Running application
To run application you need

* Setup required environment
* Clone repository
```
git clone https://bitbucket.org/TRAHOMOTO/my-project-backend.git
```
* Resolve dependencies
```
npm install
```
* Specify credentials for mysql user in **config.json** file
* Create database with same name as in **config.json**
* Import Mysql schema from **db_schema.sql** using gulp task
```
gulp schema
```
* Run application
```
npm start
```
* Navigate to http://localhost:4202 from your favourite browser. 
To sign in you can use accounts
   
   1. login: **admin**, password: **admin** to access to administrator dashboard
   2. login: **user**, password: **user** - for user dashboard

## Strict WARNING!
Never ever try to use this application on production environment! It's only for demonstration purposes. 
Otherwise, your server are at risk of a CSRF attack. **Application does not contain any mechanism to prevent CSRF**
